package homework_1;// Нахождение максимальной цифры в водимом числе

import java.util.Scanner;

public class Homework_1_1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        int max = 0;
        int temp = 0;
        if (num < 0) {
            num *= -1;
        }

        while(num > 0) {
            temp = num % 10;
            if (temp > max) {
                max = temp;
            }

            if (max == 9) {
                break;
            }

            num /= 10;
        }

        System.out.println("Максимальная цифра - " + max);
    }

}
