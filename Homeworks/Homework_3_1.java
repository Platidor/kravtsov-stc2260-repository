package homework_3;// Нахождение числа в массиве

import java.util.Scanner;

public class Homework_3_1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите длину массива: ");
        int size_array = scan.nextInt();
        int array[] = new int[size_array];
        String okonch = "";
        if (size_array > 1 && size_array < 5) {
            okonch = "а";
        } else {
            if (size_array > 5) {
                okonch = "ов";
            }
        }
        System.out.println("Введите " + size_array + " элемент"+ okonch +" массива:");

        for (int i = 0; i < size_array; i++) {
            array[i] = scan.nextInt();
        }
        System.out.println("Введите число, которое надо найти: ");
        int number = scan.nextInt();
        int i = 0;
        int index = -1;
        while (i < size_array) {
            if (number == array[i]) {
                index = i;
                i = size_array;
            }
            i++;
        }
        if (index == -1) {
            System.out.println("Число не найдено");
        } else {
            System.out.println("Индекс числа в массиве: "+ index);
        }
    }
}
