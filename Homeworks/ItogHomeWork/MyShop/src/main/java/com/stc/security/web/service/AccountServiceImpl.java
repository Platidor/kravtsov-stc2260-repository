package com.stc.security.web.service;

import com.stc.security.web.model.Account;
import com.stc.security.web.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Account getAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            throw new UsernameNotFoundException("Аккаунт с таким id не существует");
        }
        //Вытаскиваем аккаунт из его оболочки
        Account account = accountOpt.get();
        return account;
    }

    @Override
    public void saveAccount(Long accountId, String role) {

        Optional<Account> AccountDto = accountRepository.findById(accountId);
        Account account = AccountDto.get();
        if (role.equals("ADMIN")) {
            account.setRole(Account.Role.ADMIN);
        } else {
            account.setRole(Account.Role.USER);
        }

        accountRepository.save(account);
    }

    @Override
    public List<Account> findAllAccounts() {
        return accountRepository.findAllByState(Account.State.CONFIRMED);
    }

    @Override
    public void deleteAccount(Long accountId) {
        Optional<Account> accountOpt = accountRepository.findById(accountId);
        if (!accountOpt.isPresent()) {
            throw new UsernameNotFoundException("Аккаунт с таким id не существует");
        }
        //Вытаскиваем аккаунт из его оболочки
        Account account = accountOpt.get();
        //меняем состояние у этого аккаунта на УДАЛЕН
        account.setState(Account.State.DELETED);
        //Сохраняем аккаунт
        accountRepository.save(account);
    }
}
