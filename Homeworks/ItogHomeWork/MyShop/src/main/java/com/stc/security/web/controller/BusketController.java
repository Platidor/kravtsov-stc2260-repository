package com.stc.security.web.controller;

import com.stc.security.web.model.Busket;

import com.stc.security.web.service.BusketService;
import com.stc.security.web.service.ProductService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;


@Controller
@RequestMapping("/busket")
public class BusketController {

    private final BusketService busketService;
    private final ProductService productService;

    public BusketController(BusketService busketService, ProductService productService) {
        this.busketService = busketService;
        this.productService = productService;
    }
    //http:localhost:8080/busket
    @GetMapping
    public String getAllBusket(Model model) {
        List<Busket> buskets = busketService.getAllBusket();
        Collections.sort(buskets, new BusketController.IndexComparator());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Busket> busketsList = busketService.getBusketByNameuser(auth.getName());
        model.addAttribute("buskets",busketsList);
        return "busket";
    }

    @PostMapping(value = "/buy")
    public String buyBusketProduct() {
        int h = 0;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<Busket> buskets = busketService.getBusketByNameuser(auth.getName());
        ListIterator<Busket> busketsListIterator = buskets.listIterator();
        while (busketsListIterator.hasNext())
        {
            Busket busket = busketsListIterator.next();
            Long idProduct = Long.valueOf(busket.getIdproduct());;
            Long idBusket = busket.getId();
            productService.buyProduct(idProduct);
            busketService.deleteBusket(idBusket);
        }


        if (auth != null &&
                (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN")) ||
                auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("USER")))) {
            //productService.buyProduct(productId);
        }

        return "redirect:/products";
    }


    class IndexComparator implements Comparator<Busket> {
        @Override
        public int compare(Busket a, Busket b) {
            return a.getId() < b.getId() ? -1 : a.getId() == b.getId() ? 0 : 1;
        }
    }
}
