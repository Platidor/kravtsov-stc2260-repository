package com.stc.security.web.controller;

import com.stc.security.web.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.stc.security.web.service.ProductService;

import javax.validation.Valid;

@Controller
@RequestMapping("/createproduct")
public class CreateProductController {

    private final ProductService productService;

    @Autowired
    public CreateProductController(ProductService productService) {
        this.productService = productService;
    }
    //http:localhost:8080/createproduct

    @GetMapping
    public String getCreatePage() {
        return "createproduct";
    }

    
    @PostMapping(value = "/create")
    public String createProduct(@Valid ProductDto dto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(dto);
            return "create";
        }

        productService.createProduct(dto);
        return "redirect:/productschange";
    }

}
