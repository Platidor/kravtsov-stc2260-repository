package com.stc.security.web.repositories;

import com.stc.security.web.model.Busket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusketRepository extends JpaRepository<Busket,Long> {


}