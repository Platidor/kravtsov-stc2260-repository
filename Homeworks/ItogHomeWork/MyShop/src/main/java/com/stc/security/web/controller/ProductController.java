package com.stc.security.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.stc.security.web.model.Product;
import com.stc.security.web.service.ProductService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    //http:localhost:8080/products
    @GetMapping
    public String getAllProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        Collections.sort(products, new IndexComparator());
        model.addAttribute("products",products);
        return "products";
    }
    @PostMapping(value = "/{changeproduct-id}/buy")
    public String buyProduct(@PathVariable("changeproduct-id") Long productId ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getPrincipal() != "anonymousUser") {
            productService.buyProduct(productId);
        }
        //productService.buyProduct(productId);
        return "redirect:/products";
    }

    @PostMapping(value = "/{changeproduct-id}/basket")
    public String basketProduct(@PathVariable("changeproduct-id") Long productId ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getPrincipal() != "anonymousUser") {
            productService.basketProduct(productId);
        }

        return "redirect:/products";
    }
    class IndexComparator implements Comparator<Product> {
        @Override
        public int compare(Product a, Product b) {
            return a.getId() < b.getId() ? -1 : a.getId() == b.getId() ? 0 : 1;
        }
    }

}
