package com.stc.security.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BusketDto {

    private String nameuser;
    private Integer idproduct;
    private String name;
    private String article;
    private Integer count;
    private Integer price;
    private String image;
}
