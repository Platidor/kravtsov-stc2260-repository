package com.stc.security.web.service;

import com.stc.security.web.model.Busket;
import com.stc.security.web.model.Product;

import java.util.List;

public interface BusketService {
    List<Busket> getBusketByNameuser(String name);

    List<Busket> getAllBusket();

    void deleteBusket(Long busketId);
}
