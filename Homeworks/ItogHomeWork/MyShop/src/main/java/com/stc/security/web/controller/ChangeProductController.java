package com.stc.security.web.controller;

import com.stc.security.web.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.stc.security.web.model.Product;
import com.stc.security.web.service.ProductService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/changeproduct")
public class ChangeProductController {

    private final ProductService productService;

    @Autowired
    public ChangeProductController(ProductService productService) {
        this.productService = productService;
    }
    //http:localhost:8080/changeproduct

    @GetMapping
    public String getAllProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @PostMapping(value = "/{changeproduct-id}/view")
    public String getProduct(@PathVariable("changeproduct-id") Long productId, Model model) {
        Product product = productService.getProduct(productId);
        model.addAttribute("changeproduct", product);
        return "changeproduct";
    }


    @PostMapping(value = "/{changeproduct-id}/change")
    public String changeProduct(@PathVariable("changeproduct-id") Long productId, @Valid ProductDto dto) {
        productService.getProduct(productId).setName(dto.getName());
        productService.getProduct(productId).setArticle(dto.getArticle());
        productService.getProduct(productId).setCount(dto.getCount());
        productService.getProduct(productId).setPrice(dto.getPrice());
        productService.getProduct(productId).setImage(dto.getImage());
        productService.saveProduct(productId, dto);
        return "redirect:/productschange";
    }

}
