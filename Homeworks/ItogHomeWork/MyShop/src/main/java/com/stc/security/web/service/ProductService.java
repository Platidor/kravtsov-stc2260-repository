package com.stc.security.web.service;

import com.stc.security.web.dto.ProductDto;
import com.stc.security.web.model.Product;

import java.util.List;

public interface ProductService {

    Product getProduct(Long productId);

    List<Product> getAllProducts();

    void deleteProduct(Long productId);

    void saveProduct(Long productId, ProductDto dto);

    void buyProduct(Long productId);

    void basketProduct(Long productId);

    void createProduct(ProductDto dto);


}
