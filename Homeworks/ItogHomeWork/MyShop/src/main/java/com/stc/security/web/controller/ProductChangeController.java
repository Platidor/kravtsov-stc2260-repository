package com.stc.security.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.stc.security.web.model.Product;
import com.stc.security.web.service.ProductService;

import java.util.List;

@Controller
@RequestMapping("/productschange")
public class ProductChangeController {

    private final ProductService productService;

    @Autowired
    public ProductChangeController(ProductService productService) {
        this.productService = productService;
    }
    //http:localhost:8080/products-change
    @GetMapping
    public String getAllProducts(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("productschange",products);
        return "productschange";
    }

    @PostMapping(value = "/{productchange-id}/delete")
    public String deleteProduct(@PathVariable("productchange-id") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/productschange";
    }


}
