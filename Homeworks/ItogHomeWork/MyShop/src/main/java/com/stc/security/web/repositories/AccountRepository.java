package com.stc.security.web.repositories;

import com.stc.security.web.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//JpaRepository<ТИП_СУЩНОСТИ, ТИП_ИДЕНТИФИКАТОРА_СУЩНОСТИ>
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByEmail(String email);


    List<Account> findAllByState(Account.State state);
}