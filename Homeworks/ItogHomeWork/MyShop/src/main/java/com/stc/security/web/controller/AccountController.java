package com.stc.security.web.controller;

import com.stc.security.web.dto.AccountDto;
import com.stc.security.web.model.Account;
import com.stc.security.web.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
    //http:localhost:8080/accounts
    @GetMapping
    public String getAllAccounts(Model model) {
        List<Account> accounts = accountService.findAllAccounts();
        model.addAttribute("accounts", accounts);
        return "accounts";
    }

    @PostMapping(value = "/{account-id}/delete")
    public String deleteAccount(@PathVariable("account-id") Long accountId) {
        accountService.deleteAccount(accountId);
        return "redirect:/accounts";
    }

    @PostMapping(value = "/{account-id}/change")
    public String saveAccount(@PathVariable("account-id") Long accountId, @Valid AccountDto dto) {

        String role = dto.getRole();
        accountService.saveAccount(accountId, role);
        return "redirect:/accounts";
    }
}
