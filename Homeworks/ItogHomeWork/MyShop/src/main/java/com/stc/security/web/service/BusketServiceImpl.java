package com.stc.security.web.service;

import com.stc.security.web.model.Busket;
import com.stc.security.web.model.Product;
import com.stc.security.web.repositories.BusketRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class BusketServiceImpl implements BusketService {
    private final BusketRepository busketRepository;

    public BusketServiceImpl(BusketRepository busketRepository) {
        this.busketRepository = busketRepository;
    }

    @Override
    public List<Busket> getBusketByNameuser(String name) {
        List<Busket> buskets = busketRepository.findAll();
        return buskets.stream().filter(entry -> entry.getNameuser().startsWith(name)).collect(Collectors.toList());

    }

    @Override
    public void deleteBusket(Long busketId) {
        Optional<Busket> busketOpt = busketRepository.findById(busketId);
        if (!busketOpt.isPresent()) {
            throw new UsernameNotFoundException("Продукт с таким id не существует");
        }
        Busket busket = busketOpt.get();
        busketRepository.delete(busket);
    }

    @Override
    public List<Busket> getAllBusket() {
        return busketRepository.findAll();
    }
}
