package com.stc.security.web.service;

import com.stc.security.web.model.Account;

import java.util.List;

public interface AccountService {

    Account getAccount(Long accountId);

    void saveAccount(Long accountId, String role);
    List<Account> findAllAccounts();

    void deleteAccount(Long accountId);
}
