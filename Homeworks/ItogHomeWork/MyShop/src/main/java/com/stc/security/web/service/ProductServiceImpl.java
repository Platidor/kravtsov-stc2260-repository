package com.stc.security.web.service;

import com.stc.security.web.dto.ProductDto;
import com.stc.security.web.model.Account;
import com.stc.security.web.model.Busket;
import com.stc.security.web.repositories.AccountRepository;
import com.stc.security.web.repositories.BusketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.stc.security.web.model.Product;
import com.stc.security.web.repositories.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final BusketRepository busketRepository;

    private final AccountRepository accountRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, BusketRepository busketRepository, AccountRepository accountRepository) {
        this.productRepository = productRepository;
        this.busketRepository = busketRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Product getProduct(Long productId) {
        Optional<Product> productOpt = productRepository.findById(productId);
        if (!productOpt.isPresent()) {
            throw new UsernameNotFoundException("Продукт с таким id не существует");
        }
        //Вытаскиваем аккаунт из его оболочки
        Product product = productOpt.get();
        return product;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void deleteProduct(Long productId) {
        Optional<Product> productOpt = productRepository.findById(productId);
        if (!productOpt.isPresent()) {
            throw new UsernameNotFoundException("Продукт с таким id не существует");
        }
        //Вытаскиваем аккаунт из его оболочки
        Product product = productOpt.get();

        //Сохраняем аккаунт
        productRepository.delete(product);
    }

    @Override
    public void saveProduct(Long productId,ProductDto dto) {

        Optional<Product> productOpt = productRepository.findById(productId);
        Product product = productOpt.get();

        product.setName(dto.getName());
        product.setArticle(dto.getArticle());
        product.setCount(dto.getCount());
        product.setPrice(dto.getPrice());
        product.setImage(dto.getImage());
        productRepository.save(product);

    }

    @Override
    public void buyProduct(Long productId) {

        Optional<Product> productOpt = productRepository.findById(productId);
        Product product = productOpt.get();
        int count = getCountProduct(product);
        if (count > 0) {
            product.setCount(product.getCount() - 1);
        }
        productRepository.save(product);

    }

    @Override
    public void basketProduct(Long productId) {
        Optional<Product> productOpt = productRepository.findById(productId);
        Product product = productOpt.get();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Busket busket = Busket.builder()
                .nameuser(auth.getName())
                .idproduct(Math.toIntExact(productId))
                .name(product.getName())
                .article(product.getArticle())
                .count(1)
                .price(product.getPrice())
                .image(product.getImage())
                .build();

        busketRepository.save(busket);

    }

    public int getCountProduct(Product product) {
        return product.getCount();
    }

    @Override
    public void createProduct(ProductDto dto) {
        Product product = Product.builder()
                .name(dto.getName())
                .article(dto.getArticle())
                .count(dto.getCount())
                .price(dto.getPrice())
                .image(dto.getImage())
                .build();

        productRepository.save(product);
    }


}
