package com.stc.security.web.service;

import com.stc.security.web.model.Product;
import com.stc.security.web.repositories.AccountRepository;
import com.stc.security.web.repositories.BusketRepository;
import com.stc.security.web.repositories.ProductRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductServiceImplTest {

    private ProductRepository productRepository;
    private BusketRepository busketRepository;
    private AccountService accountService;
    private AccountRepository accountRepository;
    private ProductServiceImpl productServiceImpl = new ProductServiceImpl(productRepository, busketRepository, accountRepository);

    @Test
    public void getCountProduct() {
        Product product1 = new Product();
        product1.setName("Tea");
        product1.setArticle("1 kg");
        product1.setCount(100);
        product1.setPrice(200);
        product1.setImage("title.jpg");
        int actual = productServiceImpl.getCountProduct(product1);
        int expected = 100;
        assertEquals(actual,expected);
    }
}