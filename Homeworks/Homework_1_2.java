package homework_1;// Перевод числа из десятиричной в двоичную систему

import java.util.Scanner;

public class Homework_1_2 {
    public static void main(String[] args) {
        System.out.println("Введите число в десятиричной системе: ");
        Scanner scan = new Scanner(System.in);

        int num = scan.nextInt();
        int stepen = 1;
        int i = 1;
        while (num > stepen){
            stepen = stepen * 2;
            i++;
        }
        int dvoich = 0;
        while (i > 1){
            stepen = stepen / 2;
            if (num >= stepen) {
                num = num - stepen;
                dvoich = dvoich * 10 +1;
            } else {
                dvoich = dvoich * 10;
            }

            i--;
        }
        System.out.println("Число в двоичной системе - " + dvoich);
    }

}
