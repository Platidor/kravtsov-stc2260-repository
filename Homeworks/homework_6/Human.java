package homework_6;

import java.util.Objects;

public class Human {
    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String seriesPassport;
    private String numberPassport;

    public Human(String name, String lastName, String patronymic, String city, String street, String house, String flat, String seriesPassport, String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.seriesPassport = seriesPassport;
        this.numberPassport = numberPassport;
    }

    @Override
    public String toString() {
        return  lastName + ' ' + name + ' ' + patronymic + '\n' +
                "Паспорт:" + '\n' +
                "Серия:" + seriesPassport + " Номер:" + numberPassport + '\n' +
                "Город " + city + ", ул. "+ street + ", Дом " + house + ", квартира " + flat + '\n'
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return seriesPassport.equals(human.seriesPassport) && numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(seriesPassport, numberPassport);
    }
}
