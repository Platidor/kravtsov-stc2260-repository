package homework_6;

public class Main {
    public static void main(String[] args) {
        Human Serg = new Human("Сергей", "Кравцов", "Сергеевич", "Ростов-на-Дону", "18-я линия", "44", "3", "3333", "444444");
        Human Vasya = new Human("Василий", "Иванов", "Алибабаевич", "Москва", "Советская", "23", "7", "2222", "767898");
        Human Oleg = new Human("Олег", "Серов", "Яковлевич", "Липецк", "Семашко", "12", "7", "3333", "444444");
        System.out.print(Serg.toString());
        System.out.print(Vasya.toString());
        System.out.print(Oleg.toString());

        System.out.print(Serg.equals(Vasya));
        System.out.println();
        System.out.print(Serg.equals(Oleg));
        System.out.println();

    }
}
