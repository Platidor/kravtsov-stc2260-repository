package multithreading;

public class Worker extends Thread{
    private int uslovie = 0;
    private static volatile int counter = 0; // Общий счетчик на все потоки
    public Worker(String name,int k){
        super(name);
        start();
        this.uslovie = k; // Передаваемое условие
    }

    @Override
    public void run() {
        int i = 1;
        while (i <= 100) {
            if ((counter % 3) == uslovie) { // Проверка равенства остатка при делении общего счеткика на 3 с условием от каждого потока
                System.out.println("Я вылоняю свою часть работы. Я - " + Thread.currentThread().getName() + ", счетчик равен " + counter);
                counter++;
                i++;
            }

        }
    }
}
