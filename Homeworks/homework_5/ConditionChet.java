package homework_5;
// Проверка на четность числа
public class ConditionChet implements ByCondition {
    @Override
    public boolean isOk(int number) {
        boolean result = number % 2 == 0;
        return result;
    }
}
