package homework_5;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

public class Main {
    public static void main(String[] args) {
        int[] array_numbers = {10,545,623,464,24,29};
        ByCondition cond_zadanie_1 = new ConditionChet();
        printNumbers("Четные элементы массива: ",array_numbers,cond_zadanie_1);

        ByCondition cond_zadanie_2 = new ConditionSumChet();
        printNumbers("Сумма цифр каждого элемента - чётная: ",array_numbers,cond_zadanie_2);

        ByCondition cond_zadanie_3 = new ConditionCifrChet();
        printNumbers("Цифры каждого эемента - чётные: ",array_numbers,cond_zadanie_3);

        ByCondition cond_zadanie_4 = new ConditionIPalindrom();
        printNumbers("Палиндромы: ",array_numbers,cond_zadanie_4);


    }
    public static void printNumbers(String text,int[] array_numbers,ByCondition cond_zadanie){
        System.out.print(text);
        for(int i = 0;i < array_numbers.length;i++){
            if (cond_zadanie.isOk(array_numbers[i])) {
                System.out.print(array_numbers[i] + " ");
            }
        }
        System.out.println();
    }

}
