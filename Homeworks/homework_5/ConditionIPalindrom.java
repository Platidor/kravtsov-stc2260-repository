package homework_5;

public class ConditionIPalindrom implements ByCondition {
    @Override
    public boolean isOk(int number) {
        int number_work = number;
        int number_new = 0;
        int koef = 10;
        int temp;

        while(number_work > 0) {
            temp = number_work % 10;
            number_new = number_new * koef + temp ;
            number_work = number_work / 10;
        }
        if (number == number_new) {
            return true;
        } else {
            return false;
        }
    }
}
