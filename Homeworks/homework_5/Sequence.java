package homework_5;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int array_new[] = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                array_new[j] = array[i];
                j++;
            }
        }
        return array_new;
    }
}
