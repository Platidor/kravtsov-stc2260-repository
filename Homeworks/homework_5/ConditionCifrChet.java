package homework_5;

public class ConditionCifrChet implements ByCondition {
    @Override
    public boolean isOk(int number) {
        int temp = 0;
        boolean result;

        while(number > 0) {
            temp = number % 10;
            result = temp % 2 == 0;
            if (result == false) {
                return false;
            }
            number = number / 10;
        }
        return true;
    }
}
