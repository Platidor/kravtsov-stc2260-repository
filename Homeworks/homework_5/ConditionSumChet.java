package homework_5;

public class ConditionSumChet implements ByCondition {
    @Override
    public boolean isOk(int number) {
        int temp = 0;
        int sum = 0;
        while(number > 0) {
            temp = number % 10;
            sum = sum + temp;
            number = number / 10;
        }
        boolean result = sum % 2 == 0;
        return result;
    }
}
