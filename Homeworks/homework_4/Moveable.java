package homework_4;

public interface Moveable {
    void move(int x, int y);
}
