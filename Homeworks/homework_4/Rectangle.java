package homework_4;

public class Rectangle extends Figure{

    private double storona_a;

    private double storona_b;
    public Rectangle(int x, int y, double storona_a, double storona_b) {
        super(x, y);
        this.storona_a = storona_a;
        this.storona_b = storona_b;
    }

    public void getPerimeter(String chego){
        double perimeter = 2 * storona_a + 2 * storona_b;
        System.out.println("Периметр " + chego + " с координатами x=" + x + ", y=" + y + " равен: " + perimeter);
    }
}
