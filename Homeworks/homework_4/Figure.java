package homework_4;

public abstract class Figure {
    int x;

    int y;

    public Figure(int x, int y){
        this.x = x;
        this.y = y;
    }

    public abstract void getPerimeter(String chego);
}
