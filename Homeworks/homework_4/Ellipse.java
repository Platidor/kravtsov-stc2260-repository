package homework_4;

public class Ellipse extends Figure{

    private double kor_poluos;

    private double dlin_poluos;


    public Ellipse(int x, int y, double kor_poluos, double dlin_poluos) {
        super(x, y);
        this.kor_poluos = kor_poluos;
        this.dlin_poluos = dlin_poluos;
    }

    public void getPerimeter(String chego){
        double perimeter = 2 * Math.PI * Math.sqrt((kor_poluos * kor_poluos + dlin_poluos * dlin_poluos) / 2);
        System.out.println("Периметр " + chego + " с координатами x=" + x + ", y=" + y + " равен: " + perimeter);
    }

}
