package homework_4;

public class Circle extends Ellipse implements Moveable{
    public Circle(int x, int y, double kor_os) {
        super(x, y, kor_os, kor_os);
    }

    @Override
    public void move(int x, int y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }
}
