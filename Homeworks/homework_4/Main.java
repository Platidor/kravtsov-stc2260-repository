package homework_4;

public class Main {
    public static void main(String[] args) {
        //Square kvadrat_1 = new Square(4,7);
        //System.out(kvadrat_1);
        Ellipse ellipse_1 = new Ellipse(3,4,3,5);
        Circle circle_1 = new Circle(3,4,3);
        Rectangle Pryamougolnik_1 = new Rectangle(3,4,2,6);
        Square Kvadrat_1 = new Square(3,4,4);

        ellipse_1.getPerimeter("Эллипса");
        Pryamougolnik_1.getPerimeter("Прямоугольника");
        circle_1.getPerimeter("Круга");
        circle_1.move(6,2);
        circle_1.getPerimeter("Круга");
        Kvadrat_1.getPerimeter("Квадрата");
        Kvadrat_1.move(4,5);
        Kvadrat_1.getPerimeter("Квадрата");
    }
}
