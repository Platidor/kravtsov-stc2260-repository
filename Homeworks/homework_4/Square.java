package homework_4;

public class Square extends Rectangle implements Moveable{
    public Square(int x, int y, double storona) {
        super(x, y, storona, storona);
    }


    @Override
    public void move(int x, int y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }
}
