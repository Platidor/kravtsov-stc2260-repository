//import Teacher;
//import Student;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();
        Logger log = Logger.getLogger("org.hibernate");
        log.setLevel(Level.WARNING);
        try (SessionFactory sessionFactory = configuration.buildSessionFactory()) {
            int uslovie = -1;
            Scanner scan = new Scanner(System.in);

            while (uslovie != 0){
                System.out.println("�������� ��������:");
                System.out.println("1. ������� ��������� �� �������������");
                System.out.println("2. ������� �������������� �� ��������");
                System.out.println("3. ��������� ����");
                System.out.println("0. �����");

                uslovie = scan.nextInt();

                if (uslovie == 1) {
                    Session session = sessionFactory.openSession();
                    long idTeacher;
                    System.out.println("������� ID �������������: ");
                    idTeacher = scan.nextInt();

                    Teacher teachers = session.get(Teacher.class, idTeacher);
                    System.out.println("� �������������  " + teachers.getFirstName() + " " + teachers.getLastName());
                    List<Student> students = teachers.getStudents();
                    System.out.println("��������:  ");
                    for (Student student : students) {
                        System.out.println(student.getFirstName() + " " + student.getLastName());
                    }
                    System.out.println("");
                    session.close();
                }

                if (uslovie == 2) {
                    Session session = sessionFactory.openSession();
                    long idStudent;
                    System.out.println("������� ID ��������: ");
                    idStudent = scan.nextInt();

                    Student students = session.get(Student.class, idStudent);
                    System.out.println("� ��������  " + students.getFirstName() + " " + students.getLastName());
                    List<Teacher> teachers = students.getTeachers();
                    System.out.println("��������� ��������� �������������?:  ");
                    for (Teacher teacher : teachers) {
                        System.out.println(teacher.getFirstName() + " " + teacher.getLastName());
                    }
                    System.out.println("");
                    session.close();
                }
                if (uslovie == 3) {
                    Session session = sessionFactory.openSession();
                    Teacher teacher1 = Teacher.builder()
                            .firstName("Oleg")
                            .lastName("Igonin")
                            .build();
                    Teacher teacher2 = Teacher.builder()
                            .firstName("Elena")
                            .lastName("Komarova")
                            .build();
                    Student student1 = Student.builder()
                            .firstName("Serg")
                            .lastName("Taran")
                            .build();
                    Student student2 = Student.builder()
                            .firstName("Katya")
                            .lastName("Ivanova")
                            .build();
                    Student student3 = Student.builder()
                            .firstName("Tamara")
                            .lastName("Serova")
                            .build();
                    Student student4 = Student.builder()
                            .firstName("Igor")
                            .lastName("Kuleshov")
                            .build();
                    Teach_stud teach_stud1 = Teach_stud.builder()
                            .teacher_id(1L)
                            .student_id(1L)
                            .build();
                    Teach_stud teach_stud2 = Teach_stud.builder()
                            .teacher_id(1L)
                            .student_id(2L)
                            .build();
                    Teach_stud teach_stud3 = Teach_stud.builder()
                            .teacher_id(1L)
                            .student_id(3L)
                            .build();
                    Teach_stud teach_stud4 = Teach_stud.builder()
                            .teacher_id(2L)
                            .student_id(2L)
                            .build();
                    Teach_stud teach_stud5 = Teach_stud.builder()
                            .teacher_id(2L)
                            .student_id(4L)
                            .build();

                    session.save(teacher1);
                    session.save(teacher2);
                    session.save(student1);
                    session.save(student2);
                    session.save(student3);
                    session.save(student4);
                    session.save(teach_stud1);
                    session.save(teach_stud2);
                    session.save(teach_stud3);
                    session.save(teach_stud4);
                    session.save(teach_stud5);
                    session.close();
                }
            }
        }
    }
}
