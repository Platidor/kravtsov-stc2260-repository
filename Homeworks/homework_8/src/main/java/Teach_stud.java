import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Teach_stud {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "teacher_id", nullable = false)
    private long teacher_id;
    @Column(name = "student_id", nullable = false)
    private long student_id;
}
