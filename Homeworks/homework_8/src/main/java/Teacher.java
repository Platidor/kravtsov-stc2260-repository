import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @ManyToMany
    @JoinTable (name="teach_stud",
            joinColumns=@JoinColumn (name="teacher_id"),
            inverseJoinColumns=@JoinColumn(name="student_id"))
    private List<Student> students;
}
