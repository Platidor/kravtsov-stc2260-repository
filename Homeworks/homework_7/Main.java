package homework_7;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        //File file = new File("Humans.txt");
        homework_7.CrudUser crudUser = new UsersRepositoryFileImpl();

        int uslovie = -1;
        Scanner scan = new Scanner(System.in, "cp866");

        String firstName;
        String lastName;
        int age=0;
        boolean work = true;
        String workRus = "!";

        User newUser = null;
        boolean priznak = false;
        int uslovieIzm = -1;

        int idUser;

        while (uslovie != 0){
            System.out.println("Выберите действие:");
            System.out.println("1. Добавить User'а");
            System.out.println("2. Изменить User'а");
            System.out.println("3. Удалить User'а");
            System.out.println("0. Выход");

            uslovie = scan.nextInt();

            if (uslovie == 1){
                System.out.println("Введите имя: ");
                firstName = String.valueOf(scan.next());
                System.out.println("Введите фамилию: ");
                lastName =  String.valueOf(scan.next());
                System.out.println("Введите возраст: ");
                age = scan.nextInt();
                System.out.println("Работает? Yes/No" );
                while ((workRus.equals("Yes") == false) && (workRus.equals("No") == false)) {
                    workRus = scan.next();
                    if (workRus.equals("Да")) {
                        work = true;
                    }
                    if (workRus.equals("Нет") ) {
                        work = false;
                    }
                }
                crudUser.create(firstName, lastName, age, work);
                System.out.println("Пользователь добавлен");
            }

            if (uslovie == 2){
                System.out.println("Введите ID User'а: ");
                idUser = scan.nextInt();

                newUser = crudUser.findById(idUser);

                if (newUser != null) {
                    System.out.println("User с Id = " + idUser + " есть, что хотите изменить?");
                    System.out.println("1. Изменить имя");
                    System.out.println("2. Изменить фамилию");
                    System.out.println("3. Изменить возраст");
                    System.out.println("4. Изменить работу");
                    System.out.println("0. Ничего не менять");

                    uslovieIzm = scan.nextInt();

                    if (uslovieIzm == 1) {
                        System.out.println("Введите имя: ");
                        firstName = String.valueOf(scan.next());
                        newUser.setFirstName(firstName);
                    }
                    if (uslovieIzm == 2) {
                        System.out.println("Введите фамилию: ");
                        lastName =  String.valueOf(scan.next());
                        newUser.setLastName(lastName);
                    }
                    if (uslovieIzm == 3) {
                        System.out.println("Введите возраст: ");
                        age = scan.nextInt();
                        newUser.setAge(age);
                    }
                    if (uslovieIzm == 4) {
                        System.out.println("Работает? Да/Нет");
                        workRus = "!";
                        while ((workRus.equals("Да") == false) && (workRus.equals("Нет") == false)) {
                            workRus = scan.next();
                            if (workRus.equals("Да")) {
                                work = true;
                            }
                            if (workRus.equals("Нет") ) {
                                work = false;
                            }
                        }
                        newUser.setWork(work);
                    }
                    System.out.println("User'а с Id = " + idUser + " изменён");
                    crudUser.update(newUser);

                } else {
                    System.out.println("User'а с Id = " + idUser + " нет в файле");
                }

            }

            if (uslovie == 3){
                System.out.println("Введите ID User'а, которого надо удалить: ");
                idUser = scan.nextInt();
                if (crudUser.delete(idUser) == true) {
                    System.out.println("User c ID = " + idUser + " удален");
                } else {
                    System.out.println("User c ID = " + idUser + " не найден");
                }

            }

        }

    }

}
