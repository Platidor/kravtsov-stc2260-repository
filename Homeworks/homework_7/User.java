package homework_7;

public class User {
    private static int ID_COUNT = 0;
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private boolean work;


    public User(String firstName, String lastName, int age, boolean work) {
        this.id = ID_COUNT;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.work = work;

        ID_COUNT++;
    }

    public User(int id, String firstName, String lastName, int age, boolean work) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.work = work;
    }

    public static int getIdCount() {
        return ID_COUNT;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWork() {
        return work;
    }

    public static void setIdCount(int idCount) {
        ID_COUNT = idCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return "User{" +
                id + " " +
                firstName + " " +
                lastName + " " +
                age + " " +
                work +
                '}';
    }
}
