package homework_7;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Objects;

public class UsersRepositoryFileImpl implements homework_7.CrudUser {

    private String fileName = "Users.txt";

    @Override
    public User create(String firstName, String lastName, int age, boolean work) throws IOException {
        if (Files.isRegularFile(Path.of(fileName)) == false) {
            Files.createFile(Path.of(fileName));
        }

        User user = new User(lastId()+1,firstName, lastName, age, work);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(forAdd(user));
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return user;

    }

    @Override
    public User update(User user) throws IOException {
        if (Files.isRegularFile(Path.of(fileName)) == false) {
            Files.createFile(Path.of(fileName));
        }
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        List<User> users = reader.lines()
                .map(stroka -> {
                    String[] info = stroka.split("\\|");
                    int id = Integer.parseInt(info[0]);
                    if (id == user.getId()) {
                        return user;
                    } else {
                        return new User(id,info[1],info[2],Integer.parseInt(info[3]),Boolean.parseBoolean(info[4]));
                    }
                })
                .collect(Collectors.toList());
        reader.close();

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

        for (int i = 0; i < users.size(); i++) {
            writer.write(forAdd(users.get(i)));
        }
        writer.flush();
        writer.close();

        return user;
    }

    @Override
    public boolean delete(int idUser) throws IOException{
        if (Files.isRegularFile(Path.of(fileName)) == false) {
            Files.createFile(Path.of(fileName));
        }
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        String[] info;
        int id;
        boolean uslovie=false;

        List<User> users = new ArrayList<>();
        String line = reader.readLine();

        // Цикл пока не дойдем до конца файла
        while (line != null) {
            info = line.split("\\|");
            id = Integer.parseInt(info[0]);
            if (id == idUser) {
                line = reader.readLine();
                uslovie = true;
                continue;
            } else {
                users.add(new User(id,info[1],info[2],Integer.parseInt(info[3]),Boolean.parseBoolean(info[4])));
            }
//

            line = reader.readLine();
        }
        reader.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        for (int i = 0; i < users.size(); i++) {
            //System.out.print(forAdd(users.get(i)));
            writer.write(forAdd(users.get(i)));
        }
        writer.flush();
        writer.close();

        return uslovie;
    }


    // Получение последнего ID в файле
    private int lastId() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line = reader.readLine();

        String[] info;
        int id = 0;

        while (line != null) {
            info = line.split("\\|");
            //System.out.print(info[0]);
            id = Integer.parseInt(info[0]);
            line = reader.readLine();
        }
        reader.close();
        return id;
    }

    @Override
    public User findById(int idUser) throws IOException{
        if (Files.isRegularFile(Path.of(fileName)) == false) {
            Files.createFile(Path.of(fileName));
        }

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        List<User> users = reader.lines()
                .map(stroka -> {

                    String[] info = stroka.split("\\|");
                    int id = Integer.parseInt(info[0]);
                    if  (id == idUser) {
                        User newUser = new User(id, info[1], info[2], Integer.parseInt(info[3]), Boolean.parseBoolean(info[4]));
                        return newUser;
                    }
                    return null;
                })
                .collect(Collectors.toList());
        reader.close();

        users.removeIf(Objects::isNull);
        int u = 1;
        if (users.isEmpty()){
            return null;
        } else {
            return users.get(0);
        }

    }
    private String forAdd(User human) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(human.getId());
        stringBuilder.append("|");
        stringBuilder.append(human.getFirstName());
        stringBuilder.append("|");
        stringBuilder.append(human.getLastName());
        stringBuilder.append("|");
        stringBuilder.append(human.getAge());
        stringBuilder.append("|");
        stringBuilder.append(human.isWork());
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

}
