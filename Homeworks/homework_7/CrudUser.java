package homework_7;

import java.io.IOException;

public interface CrudUser {
    User create(String firstName,
                String lastName,
                int age,
                boolean work) throws IOException;

    User update(User user) throws IOException;


    boolean delete(int id) throws IOException;

    User findById(int id) throws IOException;

}
