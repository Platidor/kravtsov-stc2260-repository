package homework_3;// Сортировка массива

import java.util.Scanner;

public class Homework_3_2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите длину массива: ");
        int size_array = scan.nextInt();
        int array[] = new int[size_array];
        int array2[] = new int[size_array];
        int array3[] = new int[size_array];
        String okonch = "";
        if (size_array > 1 && size_array < 5) {
            okonch = "а";
        } else {
            if (size_array > 4) {
                okonch = "ов";
            }
        }
        System.out.println("Введите " + size_array + " элемент" + okonch + " массива:");

        for (int i = 0; i < size_array; i++) {
            array[i] = scan.nextInt();
            array2[i] =  array[i];
            array3[i] =  array[i];
        }
        System.out.println("Оригинальнный массив");
        for (int i = 0; i < size_array; i++) {
            System.out.print(array[i] + " ");
        }
        // Первый метод
        int nach = 0;
        int kon = size_array - 1;
        while (nach < kon) {
            int sdvig_nach = 0;
            int sdvig_kon = 0;
            if (array[kon] == 0) {
                sdvig_kon = 1;
            }
            if (array[nach] != 0) {
                sdvig_nach = 1;
            } else {
                if ((array[kon] != 0)) {
                    array[nach] = array[kon];
                    array[kon] = 0;
                    sdvig_nach = 1;
                    sdvig_kon = 1;
                }

            }
            nach = nach + sdvig_nach;
            kon = kon - sdvig_kon;

        }

        System.out.println(" ");
        System.out.println("Перестроенный массив первым методом за один проход");
        for (int i = 0; i < size_array; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println(" ");

        // Второй метод
        int i = 0;
        int index_null = -1;
        while (i < size_array - 1) {
            if (array2[i] == 0) {
                if (index_null == -1) {
                    index_null = i;
                }
                array2[index_null] = array2[i+1];
                array2[i+1] = 0;
                if (array2[index_null] == 0) {
                    i++;
                } else {
                    index_null = index_null +1;
                    i++;
                }
            } else {
                i++;
            }
        }
        System.out.println("Перестроенный массив вторым методом за один проход");
        for (int j = 0; j < size_array; j++) {
            System.out.print(array2[j] + " ");
        }
        System.out.println(" ");

        // Третий метод двойной прогон

        for (i = 0; i < size_array-1; i++) {
            for (int j = 0; j < size_array-i-1; j++) {
                if (array3[j] == 0) {
                    array3[j] = array3[j+1];
                    array3[j+1] = 0;
                }
            }
        }
        System.out.println("Перестроенный массив третьим методом за два прохода");
        for (int j = 0; j < size_array; j++) {
            System.out.print(array3[j] + " ");
        }
        System.out.println(" ");
    }
}
