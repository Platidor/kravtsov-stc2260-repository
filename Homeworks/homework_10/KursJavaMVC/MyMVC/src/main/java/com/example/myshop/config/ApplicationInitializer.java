package com.example.myshop.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ApplicationInitializer implements WebApplicationInitializer {
    //Веб-приложение - это набор сервлетов и содержимого.
    //ServletContext - определяет набор методов, которые сервлет использует для связи со своим контейнером сервлетов
    //Servlet - это класс, который отрабатывает запросы
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //Класс, нужный для настройки web приложения
        AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
        //Передаем настройку нашего приложения
        webApplicationContext.register(ApplicationConfig.class);
        //Контекст веб приложения
        //Объект данного класс отвечает за связывание жизненного цикла ServletContext с нашим приложением
        ContextLoaderListener contextLoaderListener = new ContextLoaderListener(webApplicationContext);
        //добавляем в сервлет контекст нашего слушателя
        servletContext.addListener(contextLoaderListener);

        //Создаем главный сервлет, который будет реагировать на все запросы
        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet(
                "dispatcher", new DispatcherServlet(webApplicationContext)
        );
        //устанавливаем приоритет
        dispatcherServlet.setLoadOnStartup(1);
        //говорим, на что реагировать (на всё)
        dispatcherServlet.addMapping("/");
    }
}
