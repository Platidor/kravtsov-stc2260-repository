package com.example.myshop.service;

import com.example.myshop.dto.AccountDto;

public interface SignUpService {

    void signUp(AccountDto dto);
}
