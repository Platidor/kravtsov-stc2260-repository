package com.example.myshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.myshop.model.Account;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

}