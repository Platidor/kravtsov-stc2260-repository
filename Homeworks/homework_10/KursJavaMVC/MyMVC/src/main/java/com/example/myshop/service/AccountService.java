package com.example.myshop.service;

import com.example.myshop.model.Account;

import java.util.List;

public interface AccountService {

    List<Account> getAllAccounts();

}
