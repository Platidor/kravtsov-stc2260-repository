package com.example.myshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.myshop.dto.AccountDto;
import com.example.myshop.service.SignUpService;

@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @Autowired
    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }
    //Мы получаем страницу регистрации
    @GetMapping
    public String getSignUpPage() {
        return "signUp";
    }

    //Отправить данные для регистрации
    @PostMapping
    public void signUp(AccountDto dto) {
        signUpService.signUp(dto);
    }
}
