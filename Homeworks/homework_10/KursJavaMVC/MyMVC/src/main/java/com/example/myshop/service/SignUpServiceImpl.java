package com.example.myshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.myshop.dto.AccountDto;
import com.example.myshop.model.Account;
import com.example.myshop.repositories.AccountRepository;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    @Autowired
    public SignUpServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(AccountDto dto) {
        Account account = Account.builder()
                .name(dto.getName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .build();

        accountRepository.save(account);
        System.out.println("Аккаунт сохранен");
    }
}
